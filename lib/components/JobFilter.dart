import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:job_finder/repos/ApiJobRepository.dart';
import 'package:job_finder/repos/CountryListRepo.dart';
import 'package:job_finder/repos/JobsRepository.dart';
import 'JobFinder.dart';

class JobFilter extends StatelessWidget {
  BuildContext context;
  JobFinderState state;
  CountryListRepo countryListRepo;
  JobsRepository _jobsRepository;
  String _description = "";
  String _page = "1";
  String _location = "";

  JobFilter(this.context, this.state, this.countryListRepo) {
    _jobsRepository = ApiJobRepository();
  }

  List<Row> checkboxRows(StateSetter setState) {
    List<Row> res = List();
    _jobsRepository.getProviders().asMap().forEach((index, p) => res.add(
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(p.getProviderName()),
              Checkbox(
                value: _jobsRepository.getProviderCheckList()[index],
                onChanged: (bool value) {
                  setState(() {
                    _jobsRepository.updateActiveList(index, value);
                  });
                },
              ),
            ],
          ),
        ));
    return res;
  }

  @override
  Widget build(context) {
    GlobalKey<AutoCompleteTextFieldState<String>> key = new GlobalKey();
    return new AlertDialog(
      content: StatefulBuilder(builder: (context, StateSetter setState) {
        return Container(
          height: 275,
          width: 300,
          child: new SingleChildScrollView(
              child: new Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TextFormField(
                initialValue: _description,
                decoration:
                    InputDecoration(helperText: 'Enter Job Description'),
                onChanged: (text) => _description = text.trim(),
              ),
              SimpleAutoCompleteTextField(
                key: key,
                decoration: new InputDecoration(helperText: "Enter Country"),
                controller: TextEditingController(),
                suggestions: countryListRepo.getCountryList(),
                textChanged: (text) => _location = text,
                clearOnSubmit: false,
                textSubmitted: (text) => _location = text.trim(),
              ),
              Divider(),
              Text("Select websites to search"),
              Column(children: checkboxRows(setState))
            ],
          )),
        );
      }),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            //
            List<bool> list = _jobsRepository.getProviderCheckList();
            if (!list.contains(true)) {
              Fluttertoast.showToast(
                  msg: "Please select a provider",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.CENTER,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0);
            } else {
              state.setState(() {
                _page = "1";
                state.onFilterSubmit(_description, _location, _page);
              });
              Navigator.of(context).pop();
            }
            //
          },
          textColor: Theme.of(context).primaryColor,
          child: const Text('Filter'),
        ),
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
          child: const Text('Close'),
        ),
      ],
    );
  }
}
