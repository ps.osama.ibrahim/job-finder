import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:job_finder/components/JobFilter.dart';
import 'package:job_finder/contracts/JobFinderContract.dart';
import 'package:job_finder/models/Job.dart';
import 'package:job_finder/presenters/JobFinderPresenter.dart';
import 'package:job_finder/repos/ApiJobRepository.dart';
import 'package:job_finder/repos/CountryListRepo.dart';
import 'package:job_finder/repos/JobsRepository.dart';
import 'package:job_finder/repos/LocalCountryListRepo.dart';

import 'JobList.dart';

class JobFinderState extends State<JobFinder> implements JobFinderContract {
  List<Job> _currentJobs;
  JobFinderPresenter _presenter;
  String _description = "";
  String _page = "1";
  String _location = "";
  CountryListRepo _countryListRepo;
  JobsRepository _jobsRepository;
  ScrollController _scrollController;
  bool _loading = true;

  JobFinderState() {
    _currentJobs = List();
    _countryListRepo = LocalCountryListRepo();
    _jobsRepository = ApiJobRepository();
    _scrollController = new ScrollController();
    _presenter = JobFinderPresenter(_jobsRepository, this);
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _page = (int.parse(_page) + 1).toString();
        Fluttertoast.showToast(
            msg: "Loading more jobs...",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.blueGrey,
            textColor: Colors.white,
            fontSize: 16.0);
        populateJobs();
      }
    });
    populateJobs();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void populateJobs() {
    _loading = true;
    Map<String, String> params = {
      "description": _description,
      "location": _location,
      "page": _page
    };
    _presenter.loadJobs(params);
  }

  @override
  void onLoadJobsError(err) {
    Fluttertoast.showToast(
        msg: err.toString(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  @override
  void onLoadJobsList(List<Job> jobs) {
    setState(() {
      _currentJobs.addAll(jobs);
      _loading = false;
    });
  }

  void onFilterSubmit(String description, String location, String page) {
    _description = description;
    _location = location;
    _page = page;
    _currentJobs.clear();
    populateJobs();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Job Finder'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.colorize),
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) =>
                    JobFilter(context, this, _countryListRepo),
              );
            },
          ),
        ],
      ),
      body: JobList(_loading, _currentJobs, _scrollController),
    );
  }
}

class JobFinder extends StatefulWidget {
  @override
  JobFinderState createState() => JobFinderState();
}
