import 'package:flutter/widgets.dart';
import 'package:job_finder/models/Job.dart';

import 'JobRow.dart';
import 'LoadingIcon.dart';

class JobList extends StatelessWidget {
  final bool _loading;
  final List<Job> _currentJobs;
  final ScrollController _scrollController;

  JobList(this._loading, this._currentJobs, this._scrollController);

  @override
  Widget build(BuildContext context) {
    if (_loading) return LoadingIcon("Loading...");
    if (_currentJobs.length == 0)
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "No Jobs Found",
            ),
          ],
        ),
      );
    return ListView.builder(
        controller: _scrollController,
        itemCount: _currentJobs.length,
        itemBuilder: (BuildContext context, int index) {
          return JobRow(_currentJobs[index]);
        });
  }
}
