import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:job_finder/models/Job.dart';
import 'package:url_launcher/url_launcher.dart';

class JobRow extends StatelessWidget {
  final _biggerFont = const TextStyle(fontSize: 18.0);
  final _imageWidth = 90.0;

  Job job;
  JobRow(this.job);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        job.jobTitle,
        style: _biggerFont,
        textDirection: TextDirection.ltr,
      ),
      subtitle: Text(
        job.companyName + "\n" + job.postDate + "\n" + job.location,
        textDirection: TextDirection.ltr,
      ),
      isThreeLine: true,
      onTap: () {
        _launchURL(job.url);
      },
      leading: Image(
        image: job.logo == null
            ? AssetImage("assets/job.png")
            : NetworkImage(job.logo),
        width: _imageWidth,
      ),
    );
  }

  void _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
