import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class LoadingIcon extends StatelessWidget {
  String _text;
  LoadingIcon(this._text);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text('${_text}\n', textDirection: TextDirection.ltr),
          new CircularProgressIndicator(),
        ],
      ),
    );
  }
}
