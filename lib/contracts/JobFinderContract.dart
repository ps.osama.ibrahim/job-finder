import 'package:job_finder/models/Job.dart';

abstract class JobFinderContract {
  void onLoadJobsList(List<Job> jobs);
  void onLoadJobsError(dynamic err);
}
