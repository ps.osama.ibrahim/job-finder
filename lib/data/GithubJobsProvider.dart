import 'dart:convert';

import 'package:job_finder/exceptions/FetchDataException.dart';
import 'package:job_finder/models/Job.dart';
import 'package:job_finder/utils/UrlUltils.dart';
import 'package:http/http.dart' as http;

import 'JobsProvider.dart';

class GithubJobFinder implements JobsProvider {
  final String _url = Uri.encodeFull("https://jobs.github.com/positions.json");
  var client;
  String _name;

  GithubJobFinder() {
    _name = "Github";
    client = http.Client();
  }

  Future<List<Job>> getJobs(Map<String, String> params) async {
    String url = UrlUtils.AddParamsToUrl(_url, params);
    print(url);
    final response = await client.get(url);
    final statusCode = response.statusCode;
    if (statusCode != 200) {
      throw new FetchDataException(
          "An Error Occured  in Github Repo: [Status Code: $statusCode]");
    }
    return await _parseJobs(response.body);
  }

  Future<List<Job>> _parseJobs(String responseBody) async {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return await parsed.map<Job>((json) => Job.fromGithubJson(json)).toList();
  }

  @override
  String getProviderName() {
    return _name;
  }
}
