import 'package:job_finder/models/Job.dart';

abstract class JobsProvider {
  Future<List<Job>> getJobs(Map<String, String> params);
  String getProviderName();
}
