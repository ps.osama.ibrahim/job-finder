import 'package:flutter/material.dart';

import 'components/JobFinder.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Job Finder',
        theme: ThemeData(
          primaryColor: Colors.blueGrey,
        ),
        home: JobFinder());
  }
}
