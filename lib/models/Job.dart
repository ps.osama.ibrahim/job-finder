class Job {
  String id;
  String logo;
  String jobTitle;
  String companyName;
  String location;
  String postDate;
  String url;

  Job(
      {this.id,
      this.logo,
      this.jobTitle,
      this.companyName,
      this.location,
      this.postDate,
      this.url});

  factory Job.fromGithubJson(Map<String, dynamic> json) {
    return Job(
        id: json['id'],
        logo: json['company_logo'],
        jobTitle: json['title'],
        companyName: json['company'],
        location: json['location'],
        postDate: json['created_at'],
        url: json['url']);
  }

  factory Job.fromUsaJobJson(Map<String, dynamic> json) {
    return Job(
        id: json['id'],
        logo: json['company_logo'],
        jobTitle: json['title'],
        companyName: json['company'],
        location: json['location'],
        postDate: json['created_at'],
        url: json['url']);
  }
}
