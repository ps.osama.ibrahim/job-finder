import 'package:job_finder/contracts/JobFinderContract.dart';
import 'package:job_finder/repos/JobsRepository.dart';

class JobFinderPresenter {
  JobFinderContract _view;
  JobsRepository _repository;

  JobFinderPresenter(JobsRepository repo, this._view) {
    _repository = repo;
  }

  void loadJobs(Map<String, String> params) {
    _repository
        .getJobs(params)
        .then((c) => _view.onLoadJobsList(c))
        .catchError((err) => _view.onLoadJobsError(err));
  }
}
