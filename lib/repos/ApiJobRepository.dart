import 'package:job_finder/data/GithubJobsProvider.dart';
import 'package:job_finder/data/JobsProvider.dart';
import 'package:job_finder/models/Job.dart';

import 'JobsRepository.dart';

class ApiJobRepository implements JobsRepository {
  List<JobsProvider> _providers;
  static List<bool> _providersCheckList;

  ApiJobRepository() {
    _providers = List();
    setProviders();
    _providersCheckList = List<bool>.filled(_providers.length, true);
  }

  void setProviders() {
    //Add your providers here
    _providers.add(GithubJobFinder());
  }

  Future<List<Job>> getJobs(Map<String, String> params) async {
    List<JobsProvider> activeList = _getActiveProviders(_providersCheckList);
    List<Job> _jobs = List();
    await Future.wait(activeList.map((p) => p.getJobs(params)))
        .then((list) => list.forEach((jobs) => _jobs.addAll(jobs)));
    return _jobs;
  }

  List<JobsProvider> getProviders() {
    return _providers;
  }

  List<JobsProvider> _getActiveProviders(List<bool> providersCheckList) {
    List<JobsProvider> res = List();
    _providers
        .asMap()
        .forEach((index, p) => providersCheckList[index] ? res.add(p) : null);
    return res;
  }

  void updateActiveList(int index, bool status) {
    _providersCheckList[index] = status;
  }

  List<bool> getProviderCheckList() {
    return _providersCheckList;
  }
}
