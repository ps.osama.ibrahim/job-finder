import 'package:job_finder/config/Credentails.dart';
import 'package:http/http.dart' as http;

class GoogleMapsLocation {
  http.Client client = http.Client();
  String _url = Uri.encodeFull(
      "https://maps.googleapis.com/maps/api/place/autocomplete/json");
  String _key = Credentails().getGoogleApiKey();

  String addQueryParams(String url, Map<String, String> queryParams) {
    queryParams.addAll({"key": _key});
    String res = url + "?";
    queryParams.forEach((k, v) => {
          if (v != null) {res = res + "&$k=$v"}
        });
    return res;
  }
}
