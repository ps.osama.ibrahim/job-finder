import 'package:job_finder/data/JobsProvider.dart';
import 'package:job_finder/models/Job.dart';

abstract class JobsRepository {
  Future<List<Job>> getJobs(Map<String, String> params);
  List<JobsProvider> getProviders();
  List<bool> getProviderCheckList();
  void updateActiveList(int index, bool status);
}
