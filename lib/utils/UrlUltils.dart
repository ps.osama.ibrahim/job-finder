class UrlUtils {
  static String AddParamsToUrl(String url, Map<String, String> queryParams) {
    String res = url + "?";
    queryParams.forEach((k, v) => {
          if (v != null)
            {v = v.replaceAll(RegExp(' +'), '+'), res = res + "&$k=$v"}
        });
    return res;
  }
}
