import 'package:flutter_test/flutter_test.dart';
import 'package:job_finder/repos/LocalCountryListRepo.dart';

void main() {
  testWidgets('Test List of countries', (WidgetTester tester) async {
    List<String> list = LocalCountryListRepo().getCountryList();
    expect(list.isNotEmpty, true);
  });

  testWidgets('Test LocalList of countries Length',
      (WidgetTester tester) async {
    List<String> list = LocalCountryListRepo().getCountryList();
    expect(list.length, 239);
  });
}
