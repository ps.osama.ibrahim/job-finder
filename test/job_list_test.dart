import 'package:flutter_test/flutter_test.dart';
import 'package:job_finder/data/GithubJobsProvider.dart';
import 'package:job_finder/models/Job.dart';

void main() {
  test("Get Valid Empty List Of Github Jobs With Filter", () async {
    List<Job> jobs = List();
    var githubClient = GithubJobFinder();
    await githubClient.getJobs({
      "description": "Java",
      "location": "India",
      "page": "10"
    }).then((list) => jobs.addAll(list));
    expect(jobs.length, 0);
  });
}
