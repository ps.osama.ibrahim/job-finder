import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:job_finder/components/JobRow.dart';
import 'package:job_finder/components/LoadingIcon.dart';
import 'package:job_finder/models/Job.dart';

void main() {
  Job _job = Job(
      id: "1",
      companyName: "Test",
      jobTitle: "Test",
      location: "Test",
      logo: "Test",
      postDate: "Test",
      url: "Test");
  // testWidgets('Test Job Row Component Text', (WidgetTester tester) async {
  //   await tester.pumpWidget(JobRow(_job));
  //   var container = find.byType(ListTile);
  //   expect(container, findsOneWidget);
  // });

  // testWidgets('Test Loading Component Loading Circle',
  //     (WidgetTester tester) async {
  //   await tester.pumpWidget(LoadingIcon("test"));
  //   var container = find.byType(CircularProgressIndicator);
  //   expect(container, findsOneWidget);
  // });
}
