import 'package:flutter_test/flutter_test.dart';
import 'package:job_finder/data/JobsProvider.dart';
import 'package:job_finder/models/Job.dart';
import 'package:job_finder/repos/ApiJobRepository.dart';
import 'package:mockito/mockito.dart';

main() {
  test("Adding no providers yeilds empty list", () async {
    ApiJobRepository jobRepository = EmptyProviderMockRepository();
    expect(jobRepository.getJobs({}), null);
  });

  test("Adding one mock provider with yeilds empty list", () async {
    ApiJobRepository jobRepository = SingleProviderMockRepository();
    expect(jobRepository.getJobs({}), null);
  });
}

class EmptyProviderMockRepository extends Mock implements ApiJobRepository {
  List<JobsProvider> _providers;
  @override
  void setProviders() {
    return null;
  }

  List<JobsProvider> getProviders() {
    return _providers;
  }
}

class SingleProviderMockRepository extends Mock implements ApiJobRepository {
  List<JobsProvider> _providers;

  @override
  void setProviders() {
    _providers.add(MockJobsProvider());
  }
}

class MockJobsProvider extends Mock implements JobsProvider {
  String _name = "Mock";

  @override
  Future<List<Job>> getJobs(Map<String, String> params) async {
    List<Job> _jobs = List();
    return _jobs;
  }

  @override
  String getProviderName() {
    return _name;
  }
}
