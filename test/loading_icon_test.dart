import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:job_finder/components/LoadingIcon.dart';

void main() {
  testWidgets('Test Loading Component Text', (WidgetTester tester) async {
    await tester.pumpWidget(LoadingIcon("test"));
    var container = find.byType(Text);
    expect(container, findsOneWidget);
  });

  testWidgets('Test Loading Component Loading Circle',
      (WidgetTester tester) async {
    await tester.pumpWidget(LoadingIcon("test"));
    var container = find.byType(CircularProgressIndicator);
    expect(container, findsOneWidget);
  });
}
