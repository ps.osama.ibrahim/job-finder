import "dart:convert";

import "package:flutter_test/flutter_test.dart";
import "package:job_finder/data/GithubJobsProvider.dart";
import 'package:job_finder/data/JobsProvider.dart';
import 'package:job_finder/exceptions/FetchDataException.dart';
import "package:job_finder/models/Job.dart";
import "package:http/http.dart";
import "package:http/testing.dart";
import 'package:mockito/mockito.dart';

void main() {
  test("Get Valid List Of Github Jobs", () async {
    List<Job> jobs = List();
    var githubClient = GithubJobFinder();
    githubClient.client = MockClient((get) async {
      final mapJson = [
        {
          "id": "123",
          "url":
              "https://jobs.github.com/positions/99a4987e-6b50-11e8-848b-794d9c4e4459",
          "created_at": "Wed Nov 07 20:22:57 UTC 2018",
          "company": "WHQ",
          "location": "Chicago",
          "title": "Front End Developer",
          "company_logo": "1.jpg"
        }
      ];
      return Response(json.encode(mapJson), 200);
    });

    await githubClient
        .getJobs({"description": "", "location": "", "page": ""}).then(
            (list) => jobs.addAll(list));
    expect(jobs.length, 1);
  });

  test("Get Valid Empty List Of Github Jobs", () async {
    List<Job> jobs = List();
    var githubClient = GithubJobFinder();

    githubClient.client = MockClient((get) async {
      final mapJson = [{}];
      return Response(json.encode(mapJson), 200);
    });

    await githubClient
        .getJobs({"description": "", "location": "", "page": "100"}).then(
            (list) => jobs.addAll(list));
    expect(jobs.length, 1);
  });

  test("Get Exception when retrieving jobs", () async {
    var githubClient = MockGithubJobFinder();
    expect(() => githubClient.getJobs({}),
        throwsA(isInstanceOf<FetchDataException>()));
  });
}

class MockGithubJobFinder extends Mock implements JobsProvider {
  var client;
  @override
  Future<List<Job>> getJobs(Map<String, String> params) {
    throw FetchDataException("TEST");
  }

  @override
  String getProviderName() {
    return "TEST";
  }
}
