import 'package:flutter_test/flutter_test.dart';
import 'package:job_finder/utils/UrlUltils.dart';

main() {
  test("Test query stirng adder to url", () async {
    String url = "";
    Map<String, String> params = {"test": "123", "test2": "456"};
    expect(UrlUtils.AddParamsToUrl(url, params), "?&test=123&test2=456");
  });

  test("Test query stirng adder to url Empty Params", () async {
    String url = "";
    Map<String, String> params = {};
    expect(UrlUtils.AddParamsToUrl(url, params), "?");
  });
}
